export function clearFromHTML(string) {
    if ((string === null) || (string === '')) {
        return false;
    }     
    else {
        string = string.toString();
    }
    return string.replace( /(<([^>]+)>)/ig, '').trim(); 
};