import Page from "./page.mjs";

class Main extends Page {

    get code () { return $('#postform-text') };
    get codeValue () { 
        return ('git config --global user.name "New Sheriff in Town"\ngit reset $ (git commit-tree HEAD ^ {tree} -m "Legacy code")\ngit push origin master --force'); };
    
    get scrollPosition () { return $('#select2-postform-category_id-container') };
    get syntaxHighlighting () { return $('#select2-postform-format-container') };
    get allOptionsSyntaxHighlighting () { return $$("//ul[@class='select2-results__options select2-results__options--nested']//li[@class='select2-results__option']") };

    get pasteExpiration () { return $('#select2-postform-expiration-container') };
    get allOptionsExpiration () { return $$("//ul[@class='select2-results__options']//li[@role='option']") };
    
    get pasteNameTitle () { return $('#postform-name') };
    get pasteNameTitleValue () { 
        return ('how to gain dominance among developers'); };

    async submitBtn() {
        await $('.-big').click();
    }
    
    async open() {
        await super.open('https://pastebin.com/');
    };

    
};

export default new Main();