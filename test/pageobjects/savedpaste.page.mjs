import Page from "./page.mjs";

class Saved extends Page {

    get title () { return $('head title') };
    get syntax () { return $('[href="/archive/bash"]') };
    get codeMatch () { return $$("//ol[@class='bash']//li") };
};

export default new Saved();