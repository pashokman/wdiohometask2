import mainPage from "../pageobjects/main.page.mjs";
import savedPage from "../pageobjects/savedpaste.page.mjs";
import { clearFromHTML } from "../functions/clearFromHtml.mjs";

describe('Second task', () => {

    it('Code', async() => {
        await mainPage.open();
        await mainPage.code.setValue(await mainPage.codeValue);
        expect(await mainPage.code.getValue()).toEqual(await mainPage.codeValue);
    });

    it('Syntax Highlighting', async() => {
        await mainPage.scrollPosition.scrollIntoView();      
        expect(await mainPage.syntaxHighlighting.getText()).toEqual('None');
        
        await mainPage.syntaxHighlighting.click();
        for(let i = 0; i < await mainPage.allOptionsSyntaxHighlighting.length; i++) {
            if(await mainPage.allOptionsSyntaxHighlighting[i].getText() === 'Bash') {
                await mainPage.allOptionsSyntaxHighlighting[i].click();
                break;
            }
        }

        expect(await mainPage.syntaxHighlighting.getText()).toEqual('Bash');
    });

    it('Paste Expiration', async() => {
        expect(await mainPage.pasteExpiration.getText()).toEqual('Never');

        await mainPage.pasteExpiration.click();
        for(let i = 0; i < await mainPage.allOptionsExpiration.length; i++) {
            if(await mainPage.allOptionsExpiration[i].getText() === '10 Minutes') {
                await mainPage.allOptionsExpiration[i].click();
                break;
            }
        }

        expect(await mainPage.pasteExpiration.getText()).toEqual('10 Minutes');
    });

    it('Paste Name / Title', async() => {
        await mainPage.pasteNameTitle.setValue(mainPage.pasteNameTitleValue);
        expect(await mainPage.pasteNameTitle.getValue()).toEqual(mainPage.pasteNameTitleValue);
    });

    it('Save paste and check the following', async() => {
        await mainPage.submitBtn();
        expect(await savedPage.title.getText()).toHaveText(mainPage.pasteNameTitleValue);
        expect(await savedPage.syntax.getText()).toEqual('Bash');
        expect(await clearFromHTML(await savedPage
            .codeMatch[2]
            .getHTML(false)))
            .toEqual(await secondParagraph(mainPage.codeValue));
    });    
});